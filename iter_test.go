package maze

import (
	"testing"

	"gonum.org/v1/gonum/graph"
)

func TestNodes(t *testing.T) {
	width, height := 5, 5
	it := &nodes{cur: 0, len: int64(width * height)}

	out := make([]graph.Node, 0, width*height)
	for it.Next() {
		out = append(out, it.Node())
	}

	if len(out) != width*height {
		t.Fail()
		t.Log(len(out), width*height)
	}
}
