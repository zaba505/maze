package maze

import (
	"math/bits"

	"gonum.org/v1/gonum/graph"
)

// neighbors iterates over all neighbors of a node
type neighbors struct {
	id  int64
	cur int64

	width int64
	// use 4 bits for direction
	count uint8
	total uint8
}

func (it *neighbors) Next() bool {
	if it.count == 0 {
		return false
	}

	n := it.count ^ (it.count & (it.count - 1))
	switch n {
	case 0b0000_0001:
		it.cur = it.id - 1
	case 0b0000_0010:
		it.cur = it.id + it.width
	case 0b0000_0100:
		it.cur = it.id + 1
	case 0b0000_1000:
		it.cur = it.id - it.width
	}

	it.count ^= n
	return true
}

func (it *neighbors) Len() int {
	return bits.OnesCount8(it.total)
}

func (it *neighbors) Reset() {
	it.count = 0
	it.count ^= it.total
}

func (it *neighbors) Node() graph.Node {
	if it.total^it.count == 0 {
		it.Next()
	}
	return node(it.cur)
}

// nodes iterates over all nodes
type nodes struct {
	len        int64
	start, cur int64
}

func (it *nodes) Next() bool {
	it.cur++
	return it.cur != it.len+1
}

func (it *nodes) Len() int { return int(it.len - it.cur) }

func (it *nodes) Reset() { it.cur = it.start }

func (it *nodes) Node() graph.Node {
	if it.cur == it.len+1 {
		return nil
	}
	return node(it.cur - 1)
}

// edges iterates over all edges
type edges struct {
	sedges, edges []graph.WeightedEdge
	cur           graph.WeightedEdge

	reverse bool
}

func (it *edges) Next() bool {
	if len(it.sedges) == 0 {
		return false
	}

	if it.reverse {
		it.reverse = !it.reverse
		return true
	}

	it.reverse = !it.reverse
	it.cur = it.sedges[0]
	it.sedges = it.sedges[1:]
	return true
}

func (it *edges) Len() int {
	return 2 * len(it.sedges)
}

func (it *edges) Reset() {
	it.sedges = it.edges[:]
	it.cur = nil
}

func (it *edges) WeightedEdge() graph.WeightedEdge {
	if it.reverse {
		return it.cur.ReversedEdge().(edge)
	}
	return it.cur
}

func (it *edges) WeightedEdgeSlice() []graph.WeightedEdge {
	return it.sedges
}
