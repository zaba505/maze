package maze

import (
	"math/rand"
	"testing"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/topo"
)

type byID struct {
	nodes []graph.Node
}

func (s *byID) Len() int { return len(s.nodes) }

func (s *byID) Swap(i, j int) {
	tmp := (*s).nodes[i]
	(*s).nodes[i] = (*s).nodes[j]
	(*s).nodes[j] = tmp
}

func (s *byID) Less(i, j int) bool {
	return (*s).nodes[i].ID() < (*s).nodes[j].ID()
}

func TestFrom(t *testing.T) {
	neighbors := []struct {
		id int64
		ns []int64
	}{
		{id: 0, ns: []int64{3, 1}},
		{id: 1, ns: []int64{0, 4, 2}},
		{id: 2, ns: []int64{1, 5}},
		{id: 3, ns: []int64{6, 4, 0}},
		{id: 4, ns: []int64{3, 7, 5, 1}},
		{id: 5, ns: []int64{4, 8, 2}},
		{id: 6, ns: []int64{7, 3}},
		{id: 7, ns: []int64{6, 8, 4}},
		{id: 8, ns: []int64{7, 5}},
	}

	m := &connectedMaze{width: 3, height: 3}

	for _, n := range neighbors {
		ns := m.From(n.id)
		if ns.Len() != len(n.ns) {
			t.Fail()
			return
		}

		s := graph.NodesOf(ns)
		for i, id := range s {
			if id.ID() != n.ns[i] {
				t.Fail()
				t.Log(n.id, id.ID(), n.ns[i])
				return
			}
		}
	}
}

func TestGenerate(t *testing.T) {
	testMaze := new(Maze)
	err := testMaze.UnmarshalText([]byte(`11111111111
10001000001
11101110101
10001000101
11101010101
10100010101
10101110111
10001000001
11111011101
10000010001
11111111111`))
	if err != nil {
		t.Error(err)
		return
	}

	m := Generate(5, 5, 1)

	if m.width != testMaze.width || m.height != testMaze.height {
		t.Fail()
		t.Log(m.width, m.height, testMaze.width, testMaze.height)
		return
	}

	if !topo.Equal(m.Graph, testMaze.Graph) {
		t.Fail()
		return
	}
}

func TestWithKruskal(t *testing.T) {
	testMaze := new(Maze)
	err := testMaze.UnmarshalText([]byte(`11111111111
10001000001
11101110101
10001000101
11101010101
10100010101
10101110111
10001000001
11111011101
10000010001
11111111111`))
	if err != nil {
		t.Error(err)
		return
	}

	g := WithKruskal(rand.NewSource(1))
	m := g.Generate(5, 5)

	if m.width != testMaze.width || m.height != testMaze.height {
		t.Fail()
		t.Log(m.width, m.height, testMaze.width, testMaze.height)
		return
	}

	if !topo.Equal(m.Graph, testMaze.Graph) {
		t.Fail()
		return
	}
}

func BenchmarkKruskal_Small(b *testing.B) {
	gen := WithKruskal(rand.NewSource(1))

	for i := 0; i < b.N; i++ {
		m := gen.Generate(10, 10)
		if m == nil {
			b.Fail()
		}
	}
}

func BenchmarkKruskal_Medium(b *testing.B) {
	gen := WithKruskal(rand.NewSource(1))

	for i := 0; i < b.N; i++ {
		m := gen.Generate(100, 100)
		if m == nil {
			b.Fail()
		}
	}
}

func BenchmarkKruskal_Large(b *testing.B) {
	gen := WithKruskal(rand.NewSource(1))

	for i := 0; i < b.N; i++ {
		m := gen.Generate(1000, 1000)
		if m == nil {
			b.Fail()
		}
	}
}

func BenchmarkPrim_Small(b *testing.B) {
	gen := WithPrim(rand.NewSource(1))

	for i := 0; i < b.N; i++ {
		m := gen.Generate(10, 10)
		if m == nil {
			b.Fail()
		}
	}
}

func BenchmarkPrim_Medium(b *testing.B) {
	gen := WithPrim(rand.NewSource(1))

	for i := 0; i < b.N; i++ {
		m := gen.Generate(100, 100)
		if m == nil {
			b.Fail()
		}
	}
}

func BenchmarkPrim_Large(b *testing.B) {
	gen := WithPrim(rand.NewSource(1))

	for i := 0; i < b.N; i++ {
		m := gen.Generate(1000, 1000)
		if m == nil {
			b.Fail()
		}
	}
}
