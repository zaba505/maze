package cmd

import (
	"io"
	"os"
	"runtime/trace"

	"github.com/spf13/cobra"
)

func preRunEs(fs ...func(*cobra.Command, []string) error) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) (err error) {
		for _, f := range fs {
			err = f(cmd, args)
			if err != nil {
				break
			}
		}
		return
	}
}

type baseCmd struct {
	*cobra.Command
}

func (cmd *baseCmd) getCommand() *cobra.Command { return cmd.Command }

type cmder interface {
	getCommand() *cobra.Command
}

type cli struct {
	*cobra.Command

	traceOut io.Writer
}

// NewCli initializes the maze cli
func NewCli() *cli {
	c := &cli{
		Command: &cobra.Command{
			Use:   "maze",
			Short: "Generate, solve, and other utilities for working with mazes.",
		},
	}

	c.PersistentFlags().Bool("help", false, "Display help and usage.")
	c.PersistentFlags().String("trace", "", "Output runtime trace info.")

	c.PersistentFlags().MarkHidden("trace")

	c.PersistentPreRunE = checkForTracing(c)
	c.PersistentPostRunE = stopTracing(c)

	c.addCommands(
		c.newGenCmd(),
		c.newMergeCmd(),
		c.newSolveCmd(),
		c.newConvertCmd(),
		c.newDiffCmd(),
	)

	return c
}

func (c *cli) addCommands(cmds ...cmder) {
	for _, cmd := range cmds {
		c.AddCommand(cmd.getCommand())
	}
}

func checkForTracing(c *cli) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		f := cmd.Flags().Lookup("trace")
		if f == nil || !f.Changed {
			return nil
		}

		name := f.Value.String()
		path, err := getFullPath(name)
		if err != nil {
			return err
		}

		c.traceOut, err = os.Create(path)
		if err != nil {
			return err
		}

		trace.Start(c.traceOut)
		return nil
	}
}

func stopTracing(c *cli) func(*cobra.Command, []string) error {
	return func(_ *cobra.Command, _ []string) error {
		if c.traceOut == nil && !trace.IsEnabled() {
			return nil
		}
		trace.Stop()

		wc, ok := c.traceOut.(io.WriteCloser)
		if !ok {
			return nil
		}

		return wc.Close()
	}
}
