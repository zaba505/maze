package cmd

import "github.com/spf13/cobra"

type mergeCmd struct {
	*cobra.Command
}

func (cmd *mergeCmd) getCommand() *cobra.Command { return cmd.Command }

func (c *cli) newMergeCmd() cmder {
	cmd := &mergeCmd{
		Command: &cobra.Command{
			Use:    "merge",
			Short:  "Merge two mazes. They must be the same dimensions.",
			Hidden: true,
		},
	}

	return cmd
}
