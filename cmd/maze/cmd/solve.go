package cmd

import (
	"bufio"
	"io"
	"runtime/trace"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/zaba505/maze"
)

func (c *cli) newSolveCmd() *baseCmd {
	cmd := &baseCmd{
		Command: &cobra.Command{
			Use:   "solve",
			Short: "Find the shortest path between two cells in a maze.",
			Args:  cobra.MaximumNArgs(1),
		},
	}

	cmd.PreRunE = setIn
	cmd.RunE = solveMaze
	cmd.PostRunE = func(c *cobra.Command, _ []string) error {
		wc, ok := c.OutOrStdout().(io.WriteCloser)
		if !ok {
			return nil
		}
		return wc.Close()
	}

	cmd.Flags().StringP("alg", "a", "djikstra", "Specify path finding algorithm to use when solving maze.")
	cmd.Flags().Int64P("start", "s", 0, "Start cell.")
	cmd.Flags().Int64P("end", "e", -1, "End cell. If negative, then max node id is used.")

	return cmd
}

func solveMaze(cmd *cobra.Command, args []string) error {
	defer trace.StartRegion(cmd.Context(), "solve_maze").End()

	alg, start, end, err := getSolveFlags(cmd)
	if err != nil {
		return err
	}

	_, task := trace.NewTask(cmd.Context(), "decode_maze")
	r := cmd.InOrStdin()
	m, _, err := decodeMaze(r)
	task.End()
	if err != nil {
		return err
	}

	if end < 0 {
		end = m.Graph.Node(int64(m.Graph.Nodes().Len() - 1)).ID()
	}

	_, task = trace.NewTask(cmd.Context(), "find_path")
	var path []int64
	switch alg {
	case "astar":
		break
	case "dijkstra":
		fallthrough
	default:
		path = maze.Dijkstra.Path(m, start, end)
	}
	task.End()

	_, task = trace.NewTask(cmd.Context(), "format_path")
	pathStrs := make([]string, len(path))
	for i, p := range path {
		pathStrs[i] = strconv.FormatInt(p, 10)
	}
	task.End()

	_, task = trace.NewTask(cmd.Context(), "write_path")
	defer task.End()

	w := bufio.NewWriter(cmd.OutOrStdout())
	defer w.Flush()

	for _, s := range pathStrs {
		_, err = w.WriteString(s)
		if err != nil {
			return err
		}

		_, err = w.WriteRune('\n')
		if err != nil {
			return err
		}
	}

	return w.Flush()
}

func getSolveFlags(cmd *cobra.Command) (alg string, s, e int64, err error) {
	alg, err = cmd.Flags().GetString("alg")
	if err != nil {
		return
	}
	s, err = cmd.Flags().GetInt64("start")
	if err != nil {
		return
	}
	e, err = cmd.Flags().GetInt64("end")
	return
}
