package cmd

import (
	"fmt"
	"runtime/trace"

	"github.com/spf13/cobra"
	"gitlab.com/zaba505/maze"
	"gonum.org/v1/gonum/graph/topo"
)

func (c *cli) newDiffCmd() cmder {
	cmd := &baseCmd{
		Command: &cobra.Command{
			Use:          "diff",
			Short:        "Compare two mazes. Useful for checking encodings match.",
			Args:         cobra.ExactArgs(2),
			SilenceUsage: true,
		},
	}

	cmd.RunE = diffMazes

	return cmd
}

func diffMazes(cmd *cobra.Command, args []string) error {
	defer trace.StartRegion(cmd.Context(), "diff_mazes").End()

	_, task := trace.NewTask(cmd.Context(), "decode_mazes")
	mazes, err := decodeMazes(args)
	task.End()
	if err != nil {
		return err
	}

	_, task = trace.NewTask(cmd.Context(), "compare_mazes")
	defer task.End()

	a, b := mazes[0], mazes[1]
	if !topo.Equal(a.Graph, b.Graph) {
		return fmt.Errorf("%s and %s are not equal", args[0], args[1])
	}

	return nil
}

func decodeMazes(paths []string) ([]*maze.Maze, error) {
	mazes := make([]*maze.Maze, len(paths))
	for i, path := range paths {
		f, err := openFile(path)
		if err != nil {
			return mazes, err
		}

		m, _, err := decodeMaze(f)
		if err != nil {
			return mazes, err
		}

		mazes[i] = m
	}

	return mazes, nil
}
