package cmd

import (
	"bytes"
	"testing"
)

func TestGenCmd(t *testing.T) {
	ex := []byte(`1111111
1000101
1110101
1010001
1010101
1000101
1111111
`)

	var out bytes.Buffer
	c := new(cli)
	cmd := c.newGenCmd()
	cmd.Flags().Set("width", "3")
	cmd.Flags().Set("height", "3")
	cmd.Flags().Set("seed", "1")
	cmd.SetOut(&out)

	err := cmd.Execute()
	if err != nil {
		t.Error(err)
		return
	}

	if !bytes.Equal(ex, out.Bytes()) {
		t.Fail()
		return
	}
}
