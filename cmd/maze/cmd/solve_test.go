package cmd

import (
	"bytes"
	"io"
	"math/rand"
	"testing"

	"gitlab.com/zaba505/maze"
)

type wCloser struct {
	io.Writer
}

func (*wCloser) Close() error { return nil }

func noopCloser(w io.Writer) io.WriteCloser {
	return &wCloser{w}
}

func TestSolveCmd(t *testing.T) {
	m := maze.WithKruskal(rand.NewSource(1)).Generate(3, 3)
	b, err := m.MarshalText()
	if err != nil {
		t.Error(err)
		return
	}
	ex := []byte("01458")

	c := new(cli)

	var buf bytes.Buffer
	cmd := c.newSolveCmd()
	cmd.SetIn(bytes.NewBuffer(b))
	cmd.SetOut(&buf)

	err = cmd.Execute()
	if err != nil {
		t.Error(err)
		return
	}

	out := buf.Bytes()
	out = bytes.ReplaceAll(out, []byte{'\n'}, []byte{})
	if !bytes.Equal(ex, out) {
		t.Fail()
		return
	}
}
