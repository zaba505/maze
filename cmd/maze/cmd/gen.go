package cmd

import (
	"io"
	"math/rand"
	"os"
	"path/filepath"
	"runtime/trace"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/zaba505/maze"
)

func (c *cli) newGenCmd() *baseCmd {
	cmd := &baseCmd{
		Command: &cobra.Command{
			Use:     "generate",
			Aliases: []string{"gen"},
			Short:   "Generate a random maze.",
		},
	}

	cmd.PreRunE = setOut
	cmd.RunE = genMaze
	cmd.PostRunE = func(c *cobra.Command, _ []string) error {
		wc, ok := cmd.OutOrStdout().(io.WriteCloser)
		if !ok {
			return nil
		}
		return wc.Close()
	}

	cmd.PersistentFlags().Bool("help", false, "Display help and usage.")

	cmd.Flags().IntP("width", "w", 10, "Maze width")
	cmd.Flags().IntP("height", "h", 10, "Maze height")
	cmd.Flags().StringP("enc", "e", "txt", "Maze encoding. Options: txt, dot, gif, png, jpeg")
	cmd.Flags().VarP(new(outFlag), "out", "o", "Specify an output file for the maze.")
	cmd.Flags().StringP("alg", "a", "kruskal", "Specify MST algorithm to use when generating maze.")
	cmd.Flags().Int64P("seed", "s", time.Now().Unix(), "Provide a seed value for the random number generator.")

	cmd.Flags().SortFlags = false

	return cmd
}

func setOut(cmd *cobra.Command, args []string) (err error) {
	f := cmd.Flags().Lookup("out")
	if f == nil {
		return nil
	}

	out := f.Value.(*outFlag).String()
	if out == "" {
		return nil
	}

	ext := filepath.Ext(out)

	enc := cmd.Flags().Lookup("enc")
	if enc == nil {
		return nil
	}
	if !enc.Changed && ext != "" {
		cmd.Flags().Set("enc", ext[1:])
	}

	w, err := os.Create(out)
	cmd.SetOut(w)
	return err
}

func genMaze(cmd *cobra.Command, args []string) error {
	defer trace.StartRegion(cmd.Context(), "generate_maze").End()

	alg, enc, w, h, s, err := getRunFlags(cmd)
	if err != nil {
		return err
	}

	_, task := trace.NewTask(cmd.Context(), "construct_generator")

	var gen maze.Generator
	src := rand.NewSource(s)
	switch alg {
	case "kruskal":
		gen = maze.WithKruskal(src)
	case "prim":
		gen = maze.WithPrim(src)
	default:
		gen = maze.WithKruskal(src)
	}
	task.End()

	_, task = trace.NewTask(cmd.Context(), "generate_maze")
	m := gen.Generate(w, h)
	task.End()

	_, task = trace.NewTask(cmd.Context(), "encodeMaze")
	defer task.End()

	return encodeMaze(cmd.OutOrStdout(), m, enc)
}

func getRunFlags(cmd *cobra.Command) (alg, enc string, width, height int, s int64, err error) {
	alg, err = cmd.Flags().GetString("alg")
	if err != nil {
		return
	}
	enc, err = cmd.Flags().GetString("enc")
	if err != nil {
		return
	}
	width, err = cmd.Flags().GetInt("width")
	if err != nil {
		return
	}
	height, err = cmd.Flags().GetInt("height")
	if err != nil {
		return
	}
	s, err = cmd.Flags().GetInt64("seed")
	return
}
