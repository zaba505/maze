// encode.go contains utilities for encoding and decoding mazes.

package cmd

import (
	"bytes"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"io/ioutil"

	"gitlab.com/zaba505/maze"
)

func encodeMaze(w io.Writer, m *maze.Maze, format string) error {
	switch format {
	case "gif":
		return gif.Encode(w, maze.Gray(m), nil)
	case "jpeg":
		return jpeg.Encode(w, maze.Gray(m), nil)
	case "png":
		return png.Encode(w, maze.Gray(m))
	case "dot":
		b, err := m.MarshalDOT()
		if err != nil {
			return err
		}

		_, err = w.Write(b)
		return err
	default:
		b, err := m.MarshalText()
		if err != nil {
			return err
		}

		_, err = w.Write(b)
		return err
	}
}

func decodeMaze(r io.Reader) (*maze.Maze, string, error) {
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, "", err
	}

	br := bytes.NewReader(b)

	img, s, err := image.Decode(br)
	if err != nil && err != image.ErrFormat {
		return nil, "", err
	}
	if err != image.ErrFormat {
		gray, ok := img.(*image.Gray)
		if ok {
			for i, p := range gray.Pix {
				switch {
				case p == 0, p == 255:
					break
				case p < 255/2:
					gray.Pix[i] = 0
				default:
					gray.Pix[i] = 255
				}
			}

			return maze.FromGray(gray), s, nil
		}

		rect := img.Bounds()
		size := rect.Size()
		width, height := size.X, size.Y

		gray = image.NewGray(img.Bounds())
		model := gray.ColorModel()

		for i := 0; i < height; i++ {
			for j := 0; j < width; j++ {
				gray.Set(j, i, model.Convert(img.At(j, i)))
			}
		}

		return maze.FromGray(gray), s, nil
	}

	// TODO: Add DOT unmarshaling

	m := new(maze.Maze)
	err = m.UnmarshalText(b)
	return m, "txt", err
}
