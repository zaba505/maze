package cmd

import (
	"io"
	"os"
	"path/filepath"
	"runtime/trace"

	"github.com/spf13/cobra"
)

func (c *cli) newConvertCmd() *baseCmd {
	cmd := &baseCmd{
		Command: &cobra.Command{
			Use:          "convert",
			Aliases:      []string{"conv"},
			Short:        "Convert a maze from one format to another.",
			Args:         cobra.MaximumNArgs(1),
			Example:      "maze convert maze.txt -e jpeg -o maze.jpeg",
			SilenceUsage: true,
		},
	}

	cmd.Flags().StringP("enc", "e", "txt", "Encoding to convert to. Options: txt, dot, gif, png, jpeg")
	cmd.Flags().VarP(new(outFlag), "out", "o", "Specify an output file for the maze.")

	cmd.PreRunE = preRunEs(setIn, setOut)
	cmd.RunE = convertMaze
	cmd.PostRunE = func(c *cobra.Command, _ []string) error {
		wc, ok := c.OutOrStdout().(io.WriteCloser)
		if !ok {
			return nil
		}
		return wc.Close()
	}

	return cmd
}

func convertMaze(cmd *cobra.Command, args []string) error {
	defer trace.StartRegion(cmd.Context(), "convert_maze").End()

	enc, err := cmd.Flags().GetString("enc")
	if err != nil {
		return err
	}

	_, task := trace.NewTask(cmd.Context(), "decode_maze")

	r := cmd.InOrStdin()
	m, _, err := decodeMaze(r)
	task.End()

	if err != nil {
		return err
	}

	_, task = trace.NewTask(cmd.Context(), "encode_maze")
	defer task.End()
	return encodeMaze(cmd.OutOrStdout(), m, enc)
}

func setIn(cmd *cobra.Command, args []string) error {
	if len(args) == 0 {
		return nil
	}

	f, err := openFile(args[0])
	if err != nil {
		return err
	}

	cmd.SetIn(f)
	return nil
}

func openFile(name string) (*os.File, error) {
	path, err := getFullPath(name)
	if err != nil {
		return nil, err
	}

	return os.Open(path)
}

func getFullPath(path string) (string, error) {
	if filepath.IsAbs(path) {
		return path, nil
	}

	wd, err := os.Getwd()
	return filepath.Join(wd, path), err
}
