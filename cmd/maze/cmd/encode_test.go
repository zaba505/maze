package cmd

import (
	"bytes"
	"math/rand"
	"testing"

	"gitlab.com/zaba505/maze"
)

func TestEncoding(t *testing.T) {
	m := maze.WithKruskal(rand.NewSource(1)).Generate(3, 3)
	ex := []byte(`1111111
1000101
1110101
1010001
1010101
1000101
1111111
`)

	testCases := []struct {
		Name string
		From string
	}{
		{Name: "From GIF to TXT", From: "gif"},
		{Name: "From JPEG to TXT", From: "jpeg"},
		{Name: "From PNG to TXT", From: "png"},
		{Name: "From TXT to TXT", From: "txt"},
	}

	for _, testCase := range testCases {
		t.Run(testCase.Name, func(subT *testing.T) {
			var b bytes.Buffer
			err := encodeMaze(&b, m, testCase.From)
			if err != nil {
				subT.Error(err)
				return
			}

			dm, s, err := decodeMaze(&b)
			if err != nil {
				subT.Error(err)
				return
			}
			if s != testCase.From {
				subT.Fail()
				return
			}

			out, err := dm.MarshalText()
			if err != nil {
				subT.Error(err)
				return
			}

			if !bytes.Equal(ex, out) {
				subT.Fail()
				return
			}
		})
	}
}
