package cmd

import (
	"os"
	"path/filepath"
)

type outFlag string

func (f *outFlag) String() string { return string(*f) }
func (*outFlag) Type() string     { return "filename" }

func (f *outFlag) Set(s string) error {
	if s == "" || filepath.IsAbs(s) {
		return nil
	}

	wd, err := os.Getwd()
	if err != nil {
		return err
	}

	*f = outFlag(filepath.Join(wd, s))
	return nil
}
