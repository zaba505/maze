package cmd

import (
	"bytes"
	"math/rand"
	"testing"

	"gitlab.com/zaba505/maze"
)

func TestConvertCmd(t *testing.T) {
	m := maze.WithKruskal(rand.NewSource(1)).Generate(3, 3)
	ex := []byte(`1111111
1000101
1110101
1010001
1010101
1000101
1111111
`)

	testCases := []struct {
		Name string
		From string
	}{
		{Name: "From GIF to TXT", From: "gif"},
		{Name: "From JPEG to TXT", From: "jpeg"},
		{Name: "From PNG to TXT", From: "png"},
		{Name: "From TXT to TXT", From: "txt"},
	}

	for _, testCase := range testCases {
		t.Run(testCase.Name, func(subT *testing.T) {
			var in bytes.Buffer
			err := encodeMaze(&in, m, testCase.From)
			if err != nil {
				subT.Error(err)
				return
			}

			var out bytes.Buffer
			c := new(cli)
			cmd := c.newConvertCmd()
			cmd.Flags().Set("enc", "txt")
			cmd.SetIn(&in)
			cmd.SetOut(&out)

			err = cmd.Execute()
			if err != nil {
				subT.Error(err)
				return
			}

			if !bytes.Equal(ex, out.Bytes()) {
				subT.Fail()
				return
			}
		})
	}
}
