module maze

go 1.14

require (
	github.com/spf13/cobra v0.0.7
	gitlab.com/zaba505/maze v0.0.0
	gonum.org/v1/gonum v0.7.0
)

replace gitlab.com/zaba505/maze => ./../../
