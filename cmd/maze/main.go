// Command maze is a cli tool for working with mazes.
package main

import (
	"log"

	"maze/cmd"
)

func main() {
	cli := cmd.NewCli()

	if err := cli.Execute(); err != nil {
		log.Fatal(err)
	}
}
