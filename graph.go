package maze

import (
	"gonum.org/v1/gonum/graph"
)

// mazeBuilder implements the graph.WeightedBuilder
// interface and is optimized for the use in MST algorithms.
//
type mazeBuilder struct {
	edges  []float64
	width  int64
	height int64
}

func newMazeGraph(width, height int) *mazeBuilder {
	b := &mazeBuilder{
		edges:  make([]float64, (width-1)*height+width*(height-1)),
		width:  int64(width),
		height: int64(height),
	}

	for i := range b.edges {
		b.edges[i] = -1
	}
	return b
}

func (b *mazeBuilder) Node(id int64) graph.Node {
	return node(id)
}

func (b *mazeBuilder) Nodes() graph.Nodes {
	return &nodes{cur: 0, len: b.width * b.height}
}

func (b *mazeBuilder) From(id int64) graph.Nodes {
	tot := 0b0000_1111
	i, j := id/b.width, id%b.width

	if i == 0 || b.edges[(id-b.width)+(id/b.width)*(b.width-1)] == -1 {
		tot ^= 0b0000_1000
	}

	if j == b.width-1 || b.edges[id+((id+1)/b.width)*(b.width-1)] == -1 {
		tot ^= 0b0000_0100
	}

	if i == b.height-1 || b.edges[id+((id+b.width)/b.width)*(b.width-1)] == -1 {
		tot ^= 0b0000_0010
	}

	if j == 0 || b.edges[(id-1)+(id/b.width)*(b.width-1)] == -1 {
		tot ^= 0b0000_0001
	}

	return &neighbors{id: id, width: b.width, count: uint8(tot), total: uint8(tot)}
}

func (b *mazeBuilder) HasEdgeBetween(xid, yid int64) bool {
	if xid > yid {
		i := xid
		xid = yid
		yid = i
	}
	return isDirectNeighbor(xid, yid, int64(b.width), int64(b.height)) && b.edges[xid+(yid/b.width)*(b.width-1)] != -1
}

func (b *mazeBuilder) Edge(uid, vid int64) graph.Edge {
	return b.WeightedEdge(uid, vid)
}

func (b *mazeBuilder) WeightedEdge(uid, vid int64) graph.WeightedEdge {
	reversed := uid > vid
	if reversed {
		n := uid
		uid = vid
		vid = n
	}

	if !isDirectNeighbor(uid, vid, b.width, b.height) {
		return nil
	}

	idx := (uid/b.width)*(2*b.width-1) + (uid % b.width)
	if uid == vid-b.width {
		idx += b.width - 1
	}

	w := b.edges[idx]
	if reversed {
		return edge{from: node(vid), to: node(uid), weight: w}
	}
	return edge{from: node(uid), to: node(vid), weight: w}
}

func (b *mazeBuilder) Weight(xid, yid int64) (float64, bool) {
	if xid == yid {
		return -1, true
	}

	e := b.WeightedEdge(xid, yid)
	if e == nil {
		return -1, false
	}
	return e.Weight(), true
}

func (b *mazeBuilder) WeightedEdgeBetween(xid, yid int64) graph.WeightedEdge {
	return b.WeightedEdge(xid, yid)
}

func (b *mazeBuilder) WeightedEdges() graph.WeightedEdges {
	// TODO: implement lazy edge iterator
	return nil
}

func (b *mazeBuilder) AddNode(n graph.Node) {}

func (b *mazeBuilder) SetWeightedEdge(e graph.WeightedEdge) {
	u, v := e.From(), e.To()
	uid, vid := u.ID(), v.ID()
	if uid > vid {
		i := uid
		uid = vid
		vid = i
	}

	// Store (uid -> vid, weight)
	idx := uid + (vid/b.width)*(b.width-1)
	b.edges[idx] = e.Weight()
}
