package maze

import (
	"bytes"
	"errors"
	"strconv"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding"
	"gonum.org/v1/gonum/graph/encoding/dot"
)

type dotter struct {
	graph.Graph
	attrs []encoding.Attribute
}

func (m dotter) Attributes() []encoding.Attribute { return m.attrs }

type attrSlice []encoding.Attribute

var nullAttrs attrSlice = []encoding.Attribute{}

func (a attrSlice) Attributes() []encoding.Attribute { return a }

func (dotter) DOTID() string { return "Maze" }

func (m dotter) DOTAttributers() (graph, node, edge encoding.Attributer) {
	return attrSlice(m.attrs), nullAttrs, nullAttrs
}

// MarshalDOT marshals the maze as a graph into the DOT language.
func (m *Maze) MarshalDOT() ([]byte, error) {
	b, err := dot.Marshal(dotter{Graph: m.Graph, attrs: m.attrs}, "", "", "  ")
	if err != nil {
		return b, err
	}

	// Chop off: '}'
	b = b[:len(b)-1]
	b = append(b, '\n')

	for i := 0; i < m.height; i++ {
		b = append(b, "  {rank=\"same\"; "...)
		for j := 0; j < m.width; j++ {
			b = append(b, strconv.Itoa(m.width*i+j)...)
			b = append(b, ", "...)
		}

		b = b[:len(b)-2]
		b = append(b, "};\n"...)
	}

	b = append(b, "}"...)
	return b, err
}

// TODO: Investigate UnmarshalDOT

// MarshalText marshals the maze into a textual form.
func (m *Maze) MarshalText() ([]byte, error) {
	width, height := 2*(m.width+1), 2*m.height+1
	b := make([]byte, width*height)

	var ni, nj int
	for i := 0; i < m.height; i++ {
		ni = 2*i + 1
		for j := 0; j < m.width; j++ {
			nj = 2*j + 1

			// Center
			b[width*ni+nj] = '0'
			b[width*ni+(nj+1)] = '1'
			b[width*ni+(nj-1)] = '1'

			// Top
			b[width*(ni-1)+(nj-1)] = '1'
			b[width*(ni-1)+nj] = '1'
			b[width*(ni-1)+(nj+1)] = '1'

			// Bottom Left and Right
			b[width*(ni+1)+(nj-1)] = '1'
			b[width*(ni+1)+nj] = '1'
			b[width*(ni+1)+(nj+1)] = '1'

			// Center Right
			if m.Connected(m.width*i+j, m.width*i+(j+1)) {
				b[width*ni+(nj+1)] = '0'
			}

			// Center Left
			if m.Connected(m.width*i+j, m.width*i+(j-1)) {
				b[width*ni+(nj-1)] = '0'
			}

			// Top Center
			if m.Connected(m.width*i+j, m.width*(i-1)+j) {
				b[width*(ni-1)+nj] = '0'
			}

			// Bottom Center
			if m.Connected(m.width*i+j, m.width*(i+1)+j) {
				b[width*(ni+1)+nj] = '0'
			}
		}
	}

	for i := 0; i < height; i++ {
		b[width*(i+1)-1] = '\n'
	}

	return b, nil
}

// UnmarshalText unmarshals the maze from its textual form.
func (m *Maze) UnmarshalText(text []byte) error {
	if m.Graph != nil {
		return errors.New("maze is already created")
	}

	lines := bytes.Split(bytes.TrimSpace(text), []byte{'\n'})

	m.height = (len(lines) - 1) / 2
	if m.height == 0 {
		return nil
	}
	m.width = (len(lines[0]) - 1) / 2

	g := newMazeGraph(m.width, m.height)
	m.Graph = g

	var id, ni, nj int
	for i := 0; i < m.height; i++ {
		ni = 2*i + 1
		for j := 0; j < m.width; j++ {
			nj = 2*j + 1
			id = m.width*i + j

			// Left
			if lines[ni][nj-1] == '0' && id > 0 {
				g.SetWeightedEdge(edge{from: node(id), to: node(id - 1)})
			}

			// Top
			if lines[ni-1][nj] == '0' && id > m.width {
				g.SetWeightedEdge(edge{from: node(id), to: node(id - m.width)})
			}

			// Right
			if lines[ni][nj+1] == '0' && id < m.width-1 {
				g.SetWeightedEdge(edge{from: node(id), to: node(id + 1)})
			}

			// Bottom
			if lines[ni+1][nj] == '0' && id < (m.height-1)*m.width {
				g.SetWeightedEdge(edge{from: node(id), to: node(id + m.width)})
			}
		}
	}

	return nil
}
