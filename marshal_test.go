package maze

import (
	"bytes"
	"math/rand"
	"testing"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding"
	"gonum.org/v1/gonum/graph/simple"
)

// Returns a maze whose path simply snake between the levels.
func simpleSnakeMaze(width, height int) *Maze {
	g := simple.NewWeightedUndirectedGraph(0, 0)
	m := &Maze{
		width:  width,
		height: height,
		Graph:  g,
		attrs:  []encoding.Attribute{{Key: "rankdir", Value: `"TB"`}},
	}

	totNodes := width * height
	nodes := make([]graph.Node, 0, totNodes)
	for i := 0; i < totNodes; i++ {
		n := g.NewNode()
		g.AddNode(n)
		nodes = append(nodes, n)
	}

	dir := 1
	for i := 0; i < height; i++ {
		row := nodes[width*i : width*(i+1)]
		if dir == -1 {
			for j := width - 1; j > 0; j-- {
				g.SetWeightedEdge(g.NewWeightedEdge(row[j], row[j-1], 1.0))
			}

			if i < height-1 {
				g.SetWeightedEdge(g.NewWeightedEdge(row[0], nodes[width*(i+1)], 1.0))
			}

			dir = 1
			continue
		}

		for j := 0; j < width-1; j++ {
			g.SetWeightedEdge(g.NewWeightedEdge(row[j], row[j+1], 1.0))
		}
		if i < height-1 {
			g.SetWeightedEdge(g.NewWeightedEdge(row[width-1], nodes[width*(i+1)+width-1], 1.0))
		}
		dir = -1
	}

	return m
}

func TestMarshalDOTRanks(t *testing.T) {
	ex := `strict graph Maze {
  graph [
    rankdir="TB"
  ];

  // Node definitions.
  0;
  1;
  2;
  3;
  4;
  5;
  6;
  7;
  8;

  // Edge definitions.
  0 -- 1;
  1 -- 2;
  2 -- 5;
  3 -- 4;
  3 -- 6;
  4 -- 5;
  6 -- 7;
  7 -- 8;

  {rank="same"; 0, 1, 2};
  {rank="same"; 3, 4, 5};
  {rank="same"; 6, 7, 8};
}`
	m := simpleSnakeMaze(3, 3)

	b, err := m.MarshalDOT()
	if err != nil {
		t.Error(err)
		return
	}

	if !bytes.Equal(b, []byte(ex)) {
		t.Fail()
		t.Logf("\nexpected:\n%s\n\ngot:\n%s", string(ex), string(b))
	}
}

func TestMarshalText(t *testing.T) {
	ex := `1111111
1000001
1111101
1000001
1011111
1000001
1111111
`

	m := simpleSnakeMaze(3, 3)

	b, err := m.MarshalText()
	if err != nil {
		t.Error(err)
		return
	}

	if !bytes.Equal(b, []byte(ex)) {
		t.Log(b, string(b))
		t.Fail()
	}
}

func TestUnmarshalText(t *testing.T) {
	ex := `1111111
1000001
1111101
1000001
1011111
1000001
1111111
`

	m := new(Maze)
	err := m.UnmarshalText([]byte(ex))
	if err != nil {
		t.Error(err)
		return
	}

	b, err := m.MarshalText()
	if err != nil {
		t.Error(err)
		return
	}

	if !bytes.Equal(b, []byte(ex)) {
		t.Log(string(b))
		t.Fail()
	}
}

func TestComplexMarshal(t *testing.T) {
	t.Run("Square", func(subT *testing.T) {
		width, height := 10, 10

		g := WithKruskal(rand.NewSource(1))
		m := g.Generate(width, height)

		ex, err := m.MarshalText()
		if err != nil {
			subT.Error(err)
			return
		}

		m2 := new(Maze)
		err = m2.UnmarshalText([]byte(ex))
		if err != nil {
			subT.Error(err)
			return
		}

		if m2.width != m.width || m2.height != m.height {
			subT.Log("mismatched dims:", m.width, m.height, m2.width, m2.height)
			subT.Fail()
			return
		}

		out, err := m2.MarshalText()
		if err != nil {
			subT.Error(err)
			return
		}

		if !bytes.Equal(ex, out) {
			subT.Log(string(out))
			subT.Fail()
		}
	})

	t.Run("Rectangular", func(subT *testing.T) {
		width, height := 11, 10

		g := WithKruskal(rand.NewSource(1))
		m := g.Generate(width, height)

		ex, err := m.MarshalText()
		if err != nil {
			subT.Error(err)
			return
		}

		m2 := new(Maze)
		err = m2.UnmarshalText([]byte(ex))
		if err != nil {
			subT.Error(err)
			return
		}

		if m2.width != m.width || m2.height != m.height {
			subT.Log("mismatched dims:", m.width, m.height, m2.width, m2.height)
			subT.Fail()
			return
		}

		out, err := m2.MarshalText()
		if err != nil {
			subT.Error(err)
			return
		}

		if !bytes.Equal(ex, out) {
			subT.Log(string(out))
			subT.Fail()
		}
	})

}

var (
	smallSnakeMaze = simpleSnakeMaze(10, 10)
	medSnakeMaze   = simpleSnakeMaze(100, 100)
	largeSnakeMaze = simpleSnakeMaze(1000, 1000)
)

func BenchmarkMarshalDOT_Small(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := smallSnakeMaze.MarshalDOT()
		if err != nil {
			b.Error(err)
			return
		}
	}
}

func BenchmarkMarshalDOT_Medium(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := medSnakeMaze.MarshalDOT()
		if err != nil {
			b.Error(err)
			return
		}
	}
}

func BenchmarkMarshalDOT_Large(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := largeSnakeMaze.MarshalDOT()
		if err != nil {
			b.Error(err)
			return
		}
	}
}

func BenchmarkMarshalText_Small(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := smallSnakeMaze.MarshalText()
		if err != nil {
			b.Error(err)
			return
		}
	}
}

func BenchmarkMarshalText_Medium(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := medSnakeMaze.MarshalText()
		if err != nil {
			b.Error(err)
			return
		}
	}
}

func BenchmarkMarshalText_Large(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := largeSnakeMaze.MarshalText()
		if err != nil {
			b.Error(err)
			return
		}
	}
}
