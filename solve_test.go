package maze

import (
	"math"
	"testing"
)

func TestAStar(t *testing.T) {
	m := simpleSnakeMaze(3, 3)

	ex := []int64{0, 1, 2, 5, 4, 3, 6, 7, 8}

	f := AStar(func(x, y int64) float64 {
		return 1
	})
	out := f.Path(m, 0, 8)

	if len(ex) != len(out) {
		t.Fail()
		return
	}

	for i, id := range ex {
		if id != out[i] {
			t.Log("expected", i, id, out[i])
			t.Fail()
			return
		}
	}
}

func BenchmarkAStar_Small(b *testing.B) {
	f := AStar(func(x, y int64) float64 {
		return math.Sqrt(float64(x*x + y*y))
	})

	for i := 0; i < b.N; i++ {
		out := f.Path(smallSnakeMaze, 0, 99)
		if len(out) == 0 {
			b.Fail()
			return
		}
	}
}

func BenchmarkAStar_Medium(b *testing.B) {
	f := AStar(func(x, y int64) float64 {
		return math.Sqrt(float64(x*x + y*y))
	})

	for i := 0; i < b.N; i++ {
		out := f.Path(medSnakeMaze, 0, 9999)
		if len(out) == 0 {
			b.Fail()
			return
		}
	}
}

func BenchmarkAStar_Large(b *testing.B) {
	f := AStar(func(x, y int64) float64 {
		return math.Sqrt(float64(x*x + y*y))
	})

	for i := 0; i < b.N; i++ {
		out := f.Path(largeSnakeMaze, 0, 999999)
		if len(out) == 0 {
			b.Fail()
			return
		}
	}
}

func TestDijkstra(t *testing.T) {
	m := simpleSnakeMaze(3, 3)

	ex := []int64{0, 1, 2, 5, 4, 3, 6, 7, 8}

	out := Dijkstra.Path(m, 0, 8)

	if len(ex) != len(out) {
		t.Fail()
		return
	}

	for i, id := range ex {
		if id != out[i] {
			t.Log("expected", i, id, out[i])
			t.Fail()
			return
		}
	}
}

func BenchmarkDijkstra_Small(b *testing.B) {
	for i := 0; i < b.N; i++ {
		out := Dijkstra.Path(smallSnakeMaze, 0, 99)
		if len(out) == 0 {
			b.Fail()
			return
		}
	}
}

func BenchmarkDijkstra_Medium(b *testing.B) {
	for i := 0; i < b.N; i++ {
		out := Dijkstra.Path(medSnakeMaze, 0, 9999)
		if len(out) == 0 {
			b.Fail()
			return
		}
	}
}

func BenchmarkDijkstra_Large(b *testing.B) {
	for i := 0; i < b.N; i++ {
		out := Dijkstra.Path(largeSnakeMaze, 0, 999999)
		if len(out) == 0 {
			b.Fail()
			return
		}
	}
}
