// Package maze provides generating, solving, and other utilities for working with mazes.
package maze

import (
	"math/rand"

	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/encoding"
	"gonum.org/v1/gonum/graph/path"
)

// Generator represents anything that can generate a maze.
type Generator interface {
	// Generate generates a maze.
	Generate(width, height int) *Maze
}

// Generate is a no configuration necessary maze generating function.
func Generate(width, height int, seed int64) *Maze {
	r := rand.New(rand.NewSource(seed))

	dst := newMazeGraph(width, height)
	m := &Maze{
		Graph:  dst,
		attrs:  []encoding.Attribute{{Key: "rankdir", Value: `"TB"`}},
		width:  width,
		height: height,
	}
	g := newGraph(r, int64(width), int64(height))

	path.Kruskal(dst, g)
	return m
}

type kruskal struct {
	*rand.Rand
}

// WithKruskal2 returns a generator which uses Kruskal's algorithm to generate mazes.
func WithKruskal(src rand.Source) Generator { return &kruskal{Rand: rand.New(src)} }

func (gen *kruskal) Generate(width, height int) *Maze {
	dst := newMazeGraph(width, height)
	m := &Maze{
		Graph:  dst,
		attrs:  []encoding.Attribute{{Key: "rankdir", Value: `"TB"`}},
		width:  width,
		height: height,
	}
	g := newGraph(gen.Rand, int64(width), int64(height))

	path.Kruskal(dst, g)

	return m
}

type prim struct {
	*rand.Rand
}

// WithPrim returns a generator which uses Prim's algorithm to generate mazes.
func WithPrim(src rand.Source) Generator { return &prim{Rand: rand.New(src)} }

func (gen *prim) Generate(width, height int) *Maze {
	dst := newMazeGraph(width, height)
	m := &Maze{
		Graph:  dst,
		attrs:  []encoding.Attribute{{Key: "rankdir", Value: `"TB"`}},
		width:  width,
		height: height,
	}
	g := newGraph(gen.Rand, int64(width), int64(height))

	path.Prim(dst, g)

	return m
}

// Maze 2D rectangular maze.
type Maze struct {
	// Graph is the underlying structure of the maze.
	// IMPORTANT! This field will be deprecated and removed in the future.
	// It is currently only exported to keep the cli tool happy.
	//
	Graph graph.Weighted

	attrs  []encoding.Attribute
	width  int
	height int
}

// Connected returns whether or not two cells in the maze
// are directly connected.
//
func (m *Maze) Connected(c1, c2 int) bool {
	return m.Graph.HasEdgeBetween(int64(c1), int64(c2))
}

// Dims returns the width and height of the maze.
func (m *Maze) Dims() (width, height int) {
	return m.width, m.height
}

// Neighbors returns all the direct neighbors of a cell.
func (m *Maze) Neighbors(id int) []int {
	nodes := m.Graph.From(int64(id))
	ids := make([]int, 0, nodes.Len())
	for nodes.Next() {
		ids = append(ids, int(nodes.Node().ID()))
	}
	return ids
}

type node int64

func (n node) ID() int64 { return int64(n) }

type connectedMaze struct {
	width, height int64
	len           int64

	edges []graph.WeightedEdge
}

func newGraph(r *rand.Rand, width, height int64) *connectedMaze {
	len := width * height

	return &connectedMaze{
		width:  width,
		height: height,
		len:    len,
		edges:  genEdges(r, int(width), int(height)),
	}
}

type edge struct {
	from, to node
	weight   float64
}

func genEdges(r *rand.Rand, width, height int) []graph.WeightedEdge {
	edges := make([]graph.WeightedEdge, (width-1)*height+width*(height-1))

	w := width - 1
	w2 := 2*width - 1
	for i := 0; i < height; i++ {
		for j := 0; j < width; j++ {
			curId := width*i + j
			if j == width-1 && i < height-1 {
				edges[i*w2+j+w] = edge{
					from:   node(curId),
					to:     node(curId + width),
					weight: r.Float64(),
				}

				continue
			}
			if j == width-1 {
				continue
			}

			edges[i*w2+j] = edge{
				from:   node(curId),
				to:     node(curId + 1),
				weight: r.Float64(),
			}

			if i == height-1 {
				continue
			}

			edges[i*w2+j+w] = edge{
				from:   node(curId),
				to:     node(curId + width),
				weight: r.Float64(),
			}
		}
	}

	return edges
}

func (e edge) From() graph.Node { return e.from }
func (e edge) To() graph.Node   { return e.to }
func (e edge) ReversedEdge() graph.Edge {
	return edge{
		from:   e.to,
		to:     e.from,
		weight: e.weight,
	}
}

func (e edge) Weight() float64 { return e.weight }

func (m *connectedMaze) Edge(uid, vid int64) graph.Edge {
	return m.WeightedEdge(uid, vid)
}

func (m *connectedMaze) From(id int64) graph.Nodes {
	tot := 0b0000_1111
	i, j := id/m.width, id%m.width

	if i == 0 {
		tot ^= 0b0000_1000
	}
	if j == m.width-1 {
		tot ^= 0b0000_0100
	}
	if i == m.height-1 {
		tot ^= 0b0000_0010
	}
	if j == 0 {
		tot ^= 0b0000_0001
	}

	return &neighbors{id: id, width: m.width, count: uint8(tot), total: uint8(tot)}
}

func (m *connectedMaze) HasEdgeBetween(xid, yid int64) bool {
	return isDirectNeighbor(xid, yid, int64(m.width), int64(m.height))
}

func (m *connectedMaze) Node(id int64) graph.Node {
	if id >= int64(m.len) {
		return nil
	}
	return node(id)
}

func (m *connectedMaze) Nodes() graph.Nodes {
	return &nodes{cur: 0, len: m.len}
}

func (m *connectedMaze) Weight(xid, yid int64) (float64, bool) {
	if xid == yid {
		return -1, true
	}

	e := m.WeightedEdge(xid, yid)
	if e == nil {
		return -1, false
	}
	return e.Weight(), true
}

func (m *connectedMaze) WeightedEdge(uid, vid int64) graph.WeightedEdge {
	reversed := uid > vid
	if reversed {
		n := uid
		uid = vid
		vid = n
	}

	if !isDirectNeighbor(uid, vid, m.width, m.height) {
		return nil
	}

	idx := (uid/m.width)*(2*m.width-1) + (uid % m.width)
	if uid == vid-m.width {
		idx += m.width - 1
	}

	e := m.edges[idx]
	if reversed {
		return e.ReversedEdge().(edge)
	}
	return e
}

func (m *connectedMaze) WeightedEdgeBetween(xid, yid int64) graph.WeightedEdge {
	return m.WeightedEdge(xid, yid)
}

func (m *connectedMaze) WeightedEdges() graph.WeightedEdges {
	es := make([]graph.WeightedEdge, 0, 2*len(m.edges))
	copy(es, m.edges)
	for _, e := range m.edges {
		es = append(es, e.ReversedEdge().(edge))
	}
	return &edges{sedges: es, edges: es}
}

func isDirectNeighbor(uid, vid, width, height int64) bool {
	tot := width * height
	if uid < 0 || vid < 0 || uid >= tot || vid >= tot {
		return false
	}
	i, j := uid/width, uid%width
	k, l := vid/width, vid%width
	return i == k && (j == l-1 || j == l+1) || j == l && (i == k-1 || i == k+1)
}
