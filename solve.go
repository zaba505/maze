package maze

import (
	"gonum.org/v1/gonum/graph"
	"gonum.org/v1/gonum/graph/path"
)

// Finder is anything that can find a path between two cells in a maze
type Finder interface {
	// Path returns the ids of the cells from start to end.
	Path(m *Maze, start, end int64) []int64
}

// FinderFn is a function which implements the Finder interface.
type FinderFn func(m *Maze, start, end int64) []int64

// Path implements the Finder interface.
func (f FinderFn) Path(m *Maze, start, end int64) []int64 {
	return f(m, start, end)
}

// AStar is a path finder implementing the A* algorithm.
func AStar(h func(x, y int64) float64) Finder {
	return FinderFn(func(m *Maze, start, end int64) []int64 {
		s, e := m.Graph.Node(start), m.Graph.Node(end)

		paths, _ := path.AStar(s, e, m.Graph, func(x, y graph.Node) float64 { return h(x.ID(), y.ID()) })

		nodes, _ := paths.To(end)
		ids := make([]int64, len(nodes))

		for i, n := range nodes {
			ids[i] = n.ID()
		}

		return ids
	})
}

// Dijkstra is a path finder implementing Dijkstra's algorithm.
var Dijkstra = FinderFn(dijkstra)

func dijkstra(m *Maze, start, end int64) []int64 {
	paths := path.DijkstraFrom(m.Graph.Node(start), m.Graph)

	nodes, _ := paths.To(end)
	ids := make([]int64, len(nodes))

	for i, n := range nodes {
		ids[i] = n.ID()
	}

	return ids
}
