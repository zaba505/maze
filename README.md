[![GoDoc](https://godoc.org/gitlab.com/zaba505/maze?status.svg)](https://godoc.org/gitlab.com/zaba505/maze)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/zaba505/maze)](https://goreportcard.com/report/gitlab.com/zaba505/maze)
[![pipeline status](https://gitlab.com/zaba505/maze/badges/master/pipeline.svg)](https://gitlab.com/zaba505/maze/-/commits/master)
[![coverage report](https://gitlab.com/zaba505/maze/badges/master/coverage.svg)](https://gitlab.com/zaba505/maze/-/commits/master)

# maze

maze is a package for working with mazes. Some utilities provided are:
maze generation, solving, and marshaling/unmarshaling.
