package maze

import (
	"math/rand"
	"testing"
)

func TestGray(t *testing.T) {
	t.Run("Square", func(subT *testing.T) {
		width, height := 10, 10

		g := WithKruskal(rand.NewSource(1))
		m := g.Generate(width, height)

		exImg := Gray(m)
		ep := exImg.Pix
		er := exImg.Rect

		m2 := FromGray(exImg)

		if m2.width != width || m2.height != height {
			subT.Log("mismatched dims:", width, height, m2.width, m2.height)
			subT.Fail()
			return
		}

		out := Gray(m2)
		op := out.Pix
		or := out.Rect

		if or != er {
			subT.Log("mismatched bounds", er, or)
			subT.Fail()
			return
		}

		ox, oy := or.Min.X, or.Min.Y
		ex, ey := er.Min.X, er.Min.Y

		var ni, nj int
		for i := 0; i < height; i++ {
			ni = 2*i + 1
			for j := 0; j < width; j++ {
				nj = 2*j + 1

				if op[out.Stride*(ni-oy)+(nj-ox)] != ep[exImg.Stride*(ni-ey)+(nj-ex)] {
					subT.Log("expected pixel", ep[exImg.Stride*(ni-ey)+(nj-ex)], out.Pix[out.Stride*(ni-oy)+(nj-ox)])
					subT.Fail()
					return
				}
			}
		}
	})

	t.Run("Rectangular", func(subT *testing.T) {
		width, height := 11, 10

		g := WithKruskal(rand.NewSource(1))
		m := g.Generate(width, height)

		exImg := Gray(m)
		ep := exImg.Pix
		er := exImg.Rect

		m2 := FromGray(exImg)

		if m2.width != width || m2.height != height {
			subT.Log("mismatched dims:", width, height, m2.width, m2.height)
			subT.Fail()
			return
		}

		out := Gray(m2)
		op := out.Pix
		or := out.Rect

		if or != er {
			subT.Log("mismatched bounds", er, or)
			subT.Fail()
			return
		}

		ox, oy := or.Min.X, or.Min.Y
		ex, ey := er.Min.X, er.Min.Y

		var ni, nj int
		for i := 0; i < height; i++ {
			ni = 2*i + 1
			for j := 0; j < width; j++ {
				nj = 2*j + 1

				if op[out.Stride*(ni-oy)+(nj-ox)] != ep[exImg.Stride*(ni-ey)+(nj-ex)] {
					subT.Log("expected pixel", ep[exImg.Stride*(ni-ey)+(nj-ex)], out.Pix[out.Stride*(ni-oy)+(nj-ox)])
					subT.Fail()
					return
				}
			}
		}
	})
}

func BenchmarkGray_Small(b *testing.B) {
	for i := 0; i < b.N; i++ {
		img := Gray(smallSnakeMaze)
		if img == nil {
			b.Fail()
		}
	}
}

func BenchmarkGray_Medium(b *testing.B) {
	for i := 0; i < b.N; i++ {
		img := Gray(medSnakeMaze)
		if img == nil {
			b.Fail()
		}
	}
}

func BenchmarkGray_Large(b *testing.B) {
	for i := 0; i < b.N; i++ {
		img := Gray(largeSnakeMaze)
		if img == nil {
			b.Fail()
		}
	}
}
