package maze

import (
	"image"
	"image/color"
)

// Gray returns a grayscale image of the maze.
func Gray(m *Maze) *image.Gray {
	img := image.NewGray(image.Rect(0, 0, 2*m.width+1, 2*m.height+1))

	var ni, nj int
	for i := 0; i < m.height; i++ {
		ni = 2*i + 1
		for j := 0; j < m.width; j++ {
			nj = 2*j + 1

			// Center
			img.SetGray(nj, ni, color.Gray{Y: 255})
			img.SetGray(nj-1, ni, color.Gray{Y: 0})
			img.SetGray(nj+1, ni, color.Gray{Y: 0})

			// Top
			img.SetGray(nj, ni-1, color.Gray{Y: 0})
			img.SetGray(nj-1, ni-1, color.Gray{Y: 0})
			img.SetGray(nj+1, ni-1, color.Gray{Y: 0})

			// Bottom Left and Right
			img.SetGray(nj, ni+1, color.Gray{Y: 0})
			img.SetGray(nj-1, ni+1, color.Gray{Y: 0})
			img.SetGray(nj+1, ni+1, color.Gray{Y: 0})

			// Center Right
			if m.Connected(m.width*i+j, m.width*i+(j+1)) {
				img.SetGray(nj+1, ni, color.Gray{Y: 255})
			}

			// Center Left
			if m.Connected(m.width*i+j, m.width*i+(j-1)) {
				img.SetGray(nj-1, ni, color.Gray{Y: 255})
			}

			// Top Center
			if m.Connected(m.width*i+j, m.width*(i-1)+j) {
				img.SetGray(nj, ni-1, color.Gray{Y: 255})
			}

			// Bottom Center
			if m.Connected(m.width*i+j, m.width*(i+1)+j) {
				img.SetGray(nj, ni+1, color.Gray{Y: 255})
			}
		}
	}

	return img
}

// FromGray extracts a maze from a grayscale image.
func FromGray(gray *image.Gray) *Maze {
	min := gray.Rect.Min
	size := gray.Rect.Size()
	width := (size.X - 1) / 2
	height := (size.Y - 1) / 2

	g := newMazeGraph(width, height)
	m := &Maze{
		Graph:  g,
		width:  width,
		height: height,
	}

	pixels := gray.Pix

	var id, ni, nj int
	for i := 0; i < height; i++ {
		ni = 2*i + 1 - min.Y
		for j := 0; j < width; j++ {
			nj = 2*j + 1 - min.X
			id = width*i + j

			// Left
			if pixels[gray.Stride*ni+(nj-1)] == 255 && id > 0 {
				g.SetWeightedEdge(edge{from: node(id), to: node(id - 1)})
			}

			// Top
			if pixels[gray.Stride*(ni-1)+nj] == 255 && id > width {
				g.SetWeightedEdge(edge{from: node(id), to: node(id - width)})
			}

			// Right
			if pixels[gray.Stride*ni+(nj+1)] == 255 && id < width-1 {
				g.SetWeightedEdge(edge{from: node(id), to: node(id + 1)})
			}

			// Bottom
			if pixels[gray.Stride*(ni+1)+nj] == 255 && id < (height-1)*width {
				g.SetWeightedEdge(edge{from: node(id), to: node(id + width)})
			}
		}
	}

	return m
}
